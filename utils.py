from django.contrib.auth.models import Permission


def get_permission(permission_name: str) -> Permission:
    app_label, codename = permission_name.split('.')
    return Permission.objects.get(content_type__app_label=app_label,
                                  codename=codename)
