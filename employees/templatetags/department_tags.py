from django import template
from django.urls import reverse_lazy

register = template.Library()


@register.inclusion_tag('departments/_actions.html', takes_context=True)
def department_actions(context, department):
    department_actions = (
        ('Edit',
         reverse_lazy('employees:departments:update',
                      kwargs={'slug': department.code}),
         'employees.change_department',
         'primary'),
        ('Delete',
         reverse_lazy('employees:departments:delete',
                      kwargs={'slug': department.code}),
         'employees.delete_department',
         'danger'),
        ('Add position',
         reverse_lazy('employees:departments:add-position',
                      kwargs={'dep_slug': department.code}),
         'employees.add_position',
         'secondary')
    )

    user = context['request'].user
    available_actions = [
        {'name': name, 'url': url, 'btn_type': btn_type}
        for name, url, perm, btn_type in department_actions
        if user.has_perm(perm)
    ]

    return {'actions': available_actions}
