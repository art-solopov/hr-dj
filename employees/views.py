import string
from random import sample

from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin)
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from .models import Department, Position, Employee
from .forms import PositionForm, UserForm


class DepartmentsList(LoginRequiredMixin, ListView):
    model = Department
    queryset = Department.objects.with_positions().default_order()
    template_name = 'departments/index.html'


class DepartmentFormMixin:
    model = Department
    template_name = 'departments/form.html'
    fields = '__all__'
    success_url = reverse_lazy('employees:departments:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        helper = FormHelper()
        helper.add_input(Submit('submit', 'Submit'))
        context['helper'] = helper
        return context


class CreateDepartment(PermissionRequiredMixin, LoginRequiredMixin,
                       DepartmentFormMixin, CreateView):
    permission_required = 'employees.add_department'


class UpdateDepartment(PermissionRequiredMixin, LoginRequiredMixin,
                       DepartmentFormMixin, UpdateView):
    permission_required = 'employees.change_department'
    slug_field = 'code'


class DeleteDepartment(PermissionRequiredMixin,
                       LoginRequiredMixin, DeleteView):
    permission_required = 'employees.delete_department'
    model = Department
    slug_field = 'code'
    template_name = 'departments/confirm_delete.html'
    success_url = reverse_lazy('employees:departments:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['back_href'] = self.request.META['HTTP_REFERER']
        return context


class AddPosition(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    permission_required = 'employees.add_position'
    form_class = PositionForm
    template_name = 'positions/form.html'
    success_url = reverse_lazy('employees:departments:list')

    def get_form_kwargs(self):
        try:
            department = Department.objects.get(code=self.kwargs['dep_slug'])
            kwargs = super().get_form_kwargs()
            kwargs['department'] = department
            return kwargs
        except Department.DoesNotExist:
            raise Http404("Department not found")


class MovePosition(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'employees.change_position'
    model = Position
    fields = ['department']
    template_name = 'positions/form.html'
    success_url = reverse_lazy('employees:departments:list')


class EmployeesList(LoginRequiredMixin, ListView):
    model = Employee
    queryset = Employee.objects.select_related('user', 'position__department')
    ordering = ('user__last_name', 'user__first_name')
    template_name = 'employees/index.html'


class EmployeeFormMixin:
    form_class = UserForm
    template_name = 'employees/form.html'
    success_url = reverse_lazy('employees:employees:list')


class CreateEmployee(LoginRequiredMixin, EmployeeFormMixin, CreateView):
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.method == 'POST':
            kwargs['password'] = self._generate_password()
        return kwargs

    def _generate_password(self) -> str:
        return ''.join(sample(string.ascii_letters + string.digits, 8))


class UpdateEmployee(LoginRequiredMixin, EmployeeFormMixin, UpdateView):
    model = User

    def get_object(self, queryset=None):
        qs = queryset or self.model.objects.all()
        try:
            return qs.get(employee__pk=self.kwargs['pk'])
        except self.model.DoesNotExist:
            raise Http404("User not found")


class EmployeeProfile(LoginRequiredMixin, DetailView):
    template_name = 'employees/profile.html'
    context_object_name = 'employee'
    model = Employee
