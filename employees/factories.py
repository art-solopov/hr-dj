import factory
from factory.django import DjangoModelFactory

from .models import Department, Position, Employee, User


class DepartmentFactory(DjangoModelFactory):
    class Meta:
        model = Department

    name = factory.Faker('company')
    code = factory.LazyAttribute(lambda a: a.name[0:3].upper())


class PositionFactory(DjangoModelFactory):
    class Meta:
        model = Position

    name = factory.Faker('word')
    department = factory.SubFactory(DepartmentFactory)


class EmployeeUserFactory(DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Faker('safe_email')
    username = factory.LazyAttribute(lambda a: a.email.split('@')[0])
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')

    @factory.post_generation
    def set_password(obj, _create, password):
        obj.set_password(password)


class EmployeeFactory(DjangoModelFactory):
    class Meta:
        model = Employee

    position = factory.SubFactory(PositionFactory)
    user = factory.SubFactory(EmployeeUserFactory)
