from django.db import models
from django.db.models import Prefetch
from django.contrib.auth.models import User


class Department(models.Model):
    code = models.CharField(max_length=31, unique=True)
    name = models.CharField(max_length=2047)

    def __str__(self):
        return f"Department: {self.code.upper()}"

    class DepartmentsQuerySet(models.QuerySet):
        def default_order(self):
            return self.order_by('code')

        def with_positions(self):
            return self.prefetch_related(
                Prefetch('position_set',
                         queryset=Position.objects.order_by('name'))
            )

    objects = models.Manager.from_queryset(DepartmentsQuerySet)()


class Position(models.Model):
    name = models.CharField(max_length=2047)
    department = models.ForeignKey(Department, on_delete=models.PROTECT)

    def __str__(self):
        return f"Position: {self.name}"


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    position = models.ForeignKey(Position, on_delete=models.PROTECT)
    photo = models.ImageField(upload_to="employee_photos/",
                              max_length=200, null=True)

    class EmployeeQuerySet(models.QuerySet):
        def with_rels(self):
            return self.prefetch_related('user', 'position')

    objects = models.Manager.from_queryset(EmployeeQuerySet)()

    def __str__(self):
        user_str = ''
        if self.user.first_name and self.user.last_name:
            user_str = f"{self.user.first_name} {self.user.last_name}"
        else:
            user_str = self.user.username

        return f"{user_str}: {self.position.name}"
