from django.forms import ModelForm, inlineformset_factory, CharField
from django.contrib.auth.models import User, Group
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from .models import Position, Employee


class PositionForm(ModelForm):
    class Meta:
        model = Position
        fields = ['name']

    def __init__(self, department=None, **kwargs):
        self.department = department
        super().__init__(**kwargs)

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))

    def save(self):
        self.instance.department = self.department
        return super().save()


class UserForm(ModelForm):
    first_name = CharField()
    last_name = CharField()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email')

    EmployeeFormSet = inlineformset_factory(
        User, Employee,
        fields=('photo', 'position'),
        can_delete=False, extra=1, max_num=1)

    def __init__(self, data=None, instance=None, password=None, files=None, **kwargs):
        super().__init__(data=data, instance=instance, files=files, **kwargs)
        self.password = password
        self.employee_formset = self.EmployeeFormSet(data=data, files=files, instance=self.instance)

        try:
            if self.instance.employee:
                self.employee_formset[0].fields['position'].disabled = True
        except ObjectDoesNotExist:
            # Do nothing if the instance employee is blank
            pass

        self.helper = FormHelper()
        self.helper.form_tag = False

        self.employee_formset.helper = FormHelper()
        self.employee_formset.helper.form_tag = False

    def is_valid(self):
        return super().is_valid() and self.employee_formset.is_valid()

    @transaction.atomic
    def save(self):
        super().save(commit=False)
        if self.password:
            self.instance.set_password(self.password)
        self.instance.save()

        department = self.instance.employee.position.department.code
        dep_group, __ = Group.objects.get_or_create(name=f'department_{department}')
        self.instance.groups.add(dep_group.pk)
        self.employee_formset.save()
        return self.instance
