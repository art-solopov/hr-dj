from django.db.models import F
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import Department, Position, Employee


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Position)
class PositionAdmin(admin.ModelAdmin):
    pass


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.with_rels()


class UserEmployeeInline(admin.StackedInline):
    model = Employee
    verbose_name_plural = 'employee'


class UserAdmin(BaseUserAdmin):
    inlines = (UserEmployeeInline,)
    list_display = (BaseUserAdmin.list_display +
                    ('position', 'department_code', 'department_name',
                     'group_names'))

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('employee',
                                 'employee__position',
                                 'employee__position__department') \
                 .prefetch_related('groups')

    def position(self, obj):
        if obj.employee is None:
            return None

        return obj.employee.position.name
    position.admin_order_field = 'employee__position__name'

    def department_code(self, obj):
        if obj.employee is None:
            return None
        return obj.employee.position.department.code.upper()
    department_code.admin_order_field = 'employee__position__department__code'

    def department_name(self, obj):
        if obj.employee is None:
            return None
        return obj.employee.position.department.name
    department_name.admin_order_field = 'employee__position__department__name'

    def group_names(self, obj):
        return ", ".join(g.name for g in obj.groups.all())


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
