from django.urls import reverse
from django.test import TestCase

from utils import get_permission
from .factories import DepartmentFactory, EmployeeFactory
from .models import Position


class WithEmployeeMixin:
    def setUp(self):
        password = 'password'
        self.employee = EmployeeFactory(user__set_password=password)
        self.client.login(username=self.employee.user.username,
                          password=password)

    def _set_permissions(self, *permission_names):
        for perm in permission_names:
            permission = get_permission(perm)
            self.employee.user.user_permissions.add(permission)


class AddPositionViewTest(WithEmployeeMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.department = DepartmentFactory()
        self.positions_count = Position.objects.count()

    def test_valid(self):
        self._set_permissions('employees.add_position')
        response = self._make_request({'name': 'Specialist'})
        self.assertRedirects(response, reverse('employees:departments:list'))

        self.assertEqual(Position.objects.count(), self.positions_count + 1)
        position = Position.objects.order_by('pk').last()
        self.assertEqual(position.department, self.department)

    def test_invalid(self):
        self._set_permissions('employees.add_position')
        response = self._make_request({'name': ''})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Position.objects.count(), self.positions_count)

    def test_forbidden(self):
        response = self._make_request({'name': 'Specialist'})

        self.assertEqual(response.status_code, 403)

    def test_department_not_found(self):
        self._set_permissions('employees.add_position')
        response = self._make_request(
            {'name': 'Specialist'},
            department_code=self.department.code + 'Z'
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(Position.objects.count(), self.positions_count)

    def _make_request(self, data, department_code=None):
        if department_code is None:
            department_code = self.department.code

        url = reverse('employees:departments:add-position',
                      kwargs={'dep_slug': department_code})
        return self.client.post(url, data)
