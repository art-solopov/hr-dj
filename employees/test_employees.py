from unittest.mock import patch, Mock
from tempfile import mkdtemp

from django.urls import reverse
from django.test import TestCase, override_settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from faker import Faker

from .factories import PositionFactory
from .forms import UserForm


@override_settings(MEDIA_ROOT=mkdtemp(prefix='hr-dj-test-media'))
class UserFormTest(TestCase):
    def setUp(self):
        self.position = PositionFactory()
        self.instance = None
        self.form_data = None
        self.form_files = None

    def test_create_valid(self):
        fake = Faker()
        self.form_data = {
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'email': fake.safe_email(),
            'username': fake.user_name(),
            'employee-TOTAL_FORMS': 1,
            'employee-INITIAL_FORMS': 0,
            'employee-MAX_NUM_FORMS': 1,
            'employee-0-position': self.position.id,
        }
        self.form_files = {
            'employee-0-photo': SimpleUploadedFile(
                'photo.png',
                open('test_data/0001.png', 'rb').read()
            )
        }
        self._build_form('Qwerty')

        self.assertTrue(self.form.is_valid())
        self.form.save()
        instance = self.form.instance
        self.assertIsNotNone(instance)
        self.assertIsNotNone(instance.pk)
        self.assertTrue(instance.check_password('Qwerty'))
        self.assertIn(f'department_{self.position.department.code}',
                      [g.name for g in instance.groups.all()])

    def test_create_invalid(self):
        fake = Faker()
        self.form_data = {
            'first_name': '',
            'last_name': '',
            'email': '',
            'username': fake.user_name(),
            'employee-TOTAL_FORMS': 1,
            'employee-INITIAL_FORMS': 0,
            'employee-MAX_NUM_FORMS': 1,
            'employee-0-position': self.position.id
        }
        self._build_form('Qwerty')

        self.assertFalse(self.form.is_valid())

    def _build_form(self, password):
        self.form = UserForm(data=self.form_data, instance=self.instance,
                             files=self.form_files,
                             password=password)


@patch('employees.views.CreateEmployee._generate_password',
       Mock(return_value='Abc123'))
@override_settings(MEDIA_ROOT=mkdtemp(prefix='hr-dj-test-media'))
class CreateEmployeeViewTest(TestCase):
    def setUp(self):
        self.position = PositionFactory()

        self.username = 'newuser'
        self.password = 'password'
        self.user = User.objects.create_user(self.username,
                                             password=self.password)
        self.client.login(username=self.username, password=self.password)

    def test_create_valid(self):
        fake = Faker()
        data = {
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'email': fake.safe_email(),
            'username': fake.user_name(),
            'employee-TOTAL_FORMS': 1,
            'employee-INITIAL_FORMS': 0,
            'employee-MAX_NUM_FORMS': 1,
            'employee-0-position': self.position.id,
            'employee-0-photo': open('test_data/0001.png', 'rb')
        }
        users_count = User.objects.count()
        response = self.client.post(reverse('employees:employees:create'), data)

        self.assertRedirects(response, reverse('employees:employees:list'))
        self.assertEquals(User.objects.count(), users_count + 1)
        user = User.objects.order_by('pk').last()
        self.assertNotEqual(user.password, '')
        self.assertTrue(user.check_password('Abc123'))
