from django.urls import path, include

from . import views as v

app_name = 'employees'

department_patterns = [
    path('', v.DepartmentsList.as_view(), name='list'),
    path('new', v.CreateDepartment.as_view(), name='create'),
    path('edit/<slug:slug>', v.UpdateDepartment.as_view(), name='update'),
    path('delete/<slug:slug>', v.DeleteDepartment.as_view(), name='delete'),
    path('add-position/<slug:dep_slug>', v.AddPosition.as_view(), name='add-position')
]

position_patterns = [
    path('move/<int:pk>', v.MovePosition.as_view(), name='move'),
]

employee_patterns = [
    path('', v.EmployeesList.as_view(), name='list'),
    path('<int:pk>', v.EmployeeProfile.as_view(), name='profile'),
    path('new', v.CreateEmployee.as_view(), name='create'),
    path('edit/<int:pk>', v.UpdateEmployee.as_view(), name='update')
]

urlpatterns = [
    path('departments/', include((department_patterns, app_name),
                                 namespace='departments')),
    path('positions/', include((position_patterns, app_name),
                               namespace='positions')),
    path('employees/', include((employee_patterns, app_name),
                               namespace='employees')),
    path('accounts/', include('django.contrib.auth.urls')),
]
