from django.urls import reverse
from django.test import TestCase

from utils import get_permission

from .models import Department
from .tests import WithEmployeeMixin


class CreateDepartmentViewTest(WithEmployeeMixin, TestCase):
    def test_valid(self):
        permission = get_permission('employees.add_department')
        self.employee.user.user_permissions.add(permission)
        departments_count = Department.objects.count()
        response = self.client.post(
            reverse('employees:departments:create'),
            {'code': 'tdep', 'name': 'Test department'}
        )
        self.assertRedirects(response, reverse('employees:departments:list'))

        self.assertEqual(Department.objects.count(), departments_count + 1)

    def test_forbidden(self):
        response = self.client.post(
            reverse('employees:departments:create'),
            {'code': 'tdep', 'name': 'Test department'}
        )

        self.assertEqual(response.status_code, 403)
