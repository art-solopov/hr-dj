const path = require('path')

const cssLoaders = [
    'style-loader',
    {
        loader: 'css-loader',
        options: {
            importLoaders: 2,
            modules: 'global'
        }
    },
    {
        loader: 'postcss-loader',
        options: {
            plugins: [require('postcss-font-family-system-ui')()]
        }
    }
]

module.exports = {
    entry: './frontend/main.js',
    output: {
        path: path.join(__dirname, 'employees', 'static'),
        filename: '[name].js'
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/,
                use: cssLoaders
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    ...cssLoaders,
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass')
                        }
                    }
                ]
            }
        ]
    }
}
